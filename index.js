//3. Find courses whose instructor is "Sir Rome" and is priced greater than or equal to 20000
db.courses.find({
        
        $and: [
           
            {instructor:{$regex: 'Sir Rome',$options: '$i'}},
            {price:{$gte:20000}}
    
        ]
    
  
    })

//2. Find courses whose instructor is "Ma'am Tine"
// a. show only its name
db.courses.find({instructor: "Ma'am Tine"},{_id:0,name:1})




//3. Find courses whose instructor is "Ma'am Miah'
// a. show only its name and price
db.courses.find({instructor: "Ma'am Miah"},{_id:0,name:1,price:1})


//4. Find courses with letter 'r' in its name and has a price greater than or equal to 20000

db.courses.find({
        
        $and: [
           
            {name:{$regex: 'r',$options: '$i'}},
            {price:{$gte:20000}}
    
        ]
    
  
    })


// 5.Find courses which isActive field is true and has a price less than or equal to 25000


db.courses.find({
        
        $and: [
           
            {isActive : true},
            {price:{$lte:25000}}
    
        ]
    
  
    }).sort({price:-1})